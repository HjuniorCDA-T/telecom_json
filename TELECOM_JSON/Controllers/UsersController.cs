﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace TELECOM_JSON.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private static string _Url = Constant.UrlApiJson.UrlJson;
        // GET: api/Users
        [HttpGet]
        public async Task<List<Entities.User>> Get()
        {
            using (HttpClient _Client = new HttpClient())
            {
                var response = await _Client.GetAsync(_Url);
                var json = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<Entities.User>>(json);
            }
        }
    }
}
